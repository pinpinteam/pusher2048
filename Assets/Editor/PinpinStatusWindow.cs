
using System.Collections.Generic;
#if TAPNATION
using Facebook.Unity.Settings;
using GameAnalyticsSDK.Editor;
using Unity.EditorCoroutines.Editor;
#endif
using Pinpin;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;
using Pinpin.Helpers;
using System.IO;
using System.Collections;
using System;
public class PinpinStatusWindow : EditorWindow
{
	[MenuItem("Pinpin/Show status Window", false, 0)]
	static void ShowStatusWindow ()
	{
		GetWindow(typeof(PinpinStatusWindow));
	}

	private Pinpin.BuildTargetList m_buildTargetList;
#if TAPNATION
	private GameAnalyticsSDK.Setup.Settings m_gameAnalyticsSettings;
	private FunGamesSdk.FunGamesSettings m_funGamesSettings;
#endif
	private Vector2 m_scrollPos;
	private string m_googleSheetUrl;
	string m_androidAdjustToken = "";
	string m_iOSAdjustToken = "";

	private void Awake ()
	{
		Loadbuildist();
	}

	private void OnFocus ()
	{
		Loadbuildist();
	}

	private void Loadbuildist ()
	{
		if (m_buildTargetList == null)
		{
			m_buildTargetList = AssetDatabase.LoadAssetAtPath<Pinpin.BuildTargetList>("Assets/Objects/BuildTargetList.asset");
		}

#if TAPNATION
		m_funGamesSettings = AssetDatabase.LoadAssetAtPath<FunGamesSdk.FunGamesSettings>("Assets/Resources/FunGamesSettings.asset");
		m_gameAnalyticsSettings = AssetDatabase.LoadAssetAtPath<GameAnalyticsSDK.Setup.Settings>("Assets/Resources/GameAnalytics/Settings.asset");
#endif
	}

#if TAPNATION
	private IEnumerator WaitForGAData ( string GAiOSGameName, string GAAndroidGameName )
	{
		while (m_gameAnalyticsSettings.Organizations == null || m_gameAnalyticsSettings.Organizations.Count == 0)
		{
			yield return null;
		}
		GameAnalyticsSDK.Setup.Organization organization = m_gameAnalyticsSettings.Organizations.Find(x => x.Name == "Pinpin Team");
		GameAnalyticsSDK.Setup.Studio studio = organization.Studios.Find(x => x.Name == "Pinpin Team");
		if (!string.IsNullOrEmpty(GAiOSGameName))
		{
			GameAnalyticsSDK.Setup.Game iOSGame = studio.Games.Find(x => x.Name == GAiOSGameName);
			if (iOSGame != null)
			{
				/*int iOSPlatformIndex = 0;
				if (!m_gameAnalyticsSettings.Platforms.Contains(RuntimePlatform.IPhonePlayer))
				{
					m_gameAnalyticsSettings.AddPlatform(RuntimePlatform.IPhonePlayer);
					iOSPlatformIndex = m_gameAnalyticsSettings.Platforms.Count - 1;
				}
				else
				{
					for (int i = 0; i < m_gameAnalyticsSettings.Platforms.Count; i++)
					{
						if (m_gameAnalyticsSettings.Platforms[i] == RuntimePlatform.IPhonePlayer)
						{
							iOSPlatformIndex = i;
						}
					}
				}
				m_gameAnalyticsSettings.SelectedPlatformOrganization[iOSPlatformIndex] = organization.Name;
				m_gameAnalyticsSettings.SelectedPlatformStudio[iOSPlatformIndex] = studio.Name;
				m_gameAnalyticsSettings.SelectedPlatformGame[iOSPlatformIndex] = iOSGame.Name;
				m_gameAnalyticsSettings.UpdateGameKey(iOSPlatformIndex, iOSGame.GameKey);
				m_gameAnalyticsSettings.UpdateSecretKey(iOSPlatformIndex, iOSGame.SecretKey);*/
				m_funGamesSettings.gameAnalyticsIosGameKey = iOSGame.GameKey;
				m_funGamesSettings.gameAnalyticsIosSecretKey = iOSGame.SecretKey;
			}
		}

		if (!string.IsNullOrEmpty(GAAndroidGameName))
		{
			GameAnalyticsSDK.Setup.Game AndroidGame = studio.Games.Find(x => x.Name == GAAndroidGameName);
			if (AndroidGame != null)
			{
				/*int androidPlatformIndex = 0;
				if (!m_gameAnalyticsSettings.Platforms.Contains(RuntimePlatform.IPhonePlayer))
				{
					m_gameAnalyticsSettings.AddPlatform(RuntimePlatform.IPhonePlayer);
					androidPlatformIndex = m_gameAnalyticsSettings.Platforms.Count - 1;
				}
				else
				{
					for (int i = 0; i < m_gameAnalyticsSettings.Platforms.Count; i++)
					{
						if (m_gameAnalyticsSettings.Platforms[i] == RuntimePlatform.Android)
						{
							androidPlatformIndex = i;
						}
					}
				}
				m_gameAnalyticsSettings.SelectedPlatformOrganization[androidPlatformIndex] = organization.Name;
				m_gameAnalyticsSettings.SelectedPlatformStudio[androidPlatformIndex] = studio.Name;
				m_gameAnalyticsSettings.SelectedPlatformGame[androidPlatformIndex] = AndroidGame.Name;
				m_gameAnalyticsSettings.UpdateGameKey(androidPlatformIndex, AndroidGame.GameKey);
				m_gameAnalyticsSettings.UpdateSecretKey(androidPlatformIndex, AndroidGame.SecretKey);*/
				m_funGamesSettings.gameAnalyticsAndroidGameKey = AndroidGame.GameKey;
				m_funGamesSettings.gameAnalyticsAndroidSecretKey = AndroidGame.SecretKey;
			}
		}
	}
#endif


	private void OnGUI ()
	{
		GUIStyle errorStyle = new GUIStyle(EditorStyles.label);
		errorStyle.normal.textColor = new Color(0.8627452f, 0.2f, 0.1882353f);
		errorStyle.font = EditorStyles.boldFont;

		GUIStyle goodStyle = new GUIStyle(EditorStyles.label);
		goodStyle.normal.textColor = new Color(48f / 255f, 209f / 255f, 88f / 255f);
		goodStyle.font = EditorStyles.boldFont;


		EditorGUILayout.BeginVertical(EditorStyles.helpBox);

		EditorGUILayout.BeginHorizontal();

		GUILayout.Label("Project phase :");
		EditorGUI.BeginChangeCheck();
		m_buildTargetList.m_projectPhase = (BuildTargetList.ProjectPhase)EditorGUILayout.Popup((int)m_buildTargetList.m_projectPhase, Enum.GetNames(typeof(BuildTargetList.ProjectPhase)), GUILayout.ExpandWidth(false));

		if (EditorGUI.EndChangeCheck())
		{
			EditorUtility.SetDirty(m_buildTargetList);
		}

		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();

		GUILayout.Label("Bundle Identifier :");

		if (PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.iOS) == PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.Android))
		{
			if (string.IsNullOrEmpty(PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.iOS)))
				GUILayout.Label("Not Set", errorStyle, GUILayout.ExpandWidth(false));
			else if (PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.iOS) == "com.pinpinteam.gametemplate")
				GUILayout.Label(PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.iOS), errorStyle, GUILayout.ExpandWidth(false));
			else
				GUILayout.Label(PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.iOS), goodStyle, GUILayout.ExpandWidth(false));
		}
		else
		{
			GUILayout.Label("iOS : ", GUILayout.ExpandWidth(false));
			if (string.IsNullOrEmpty(PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.iOS)))
				GUILayout.Label("Not Set", errorStyle, GUILayout.ExpandWidth(false));
			else if (PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.iOS) == "com.pinpinteam.gametemplate")
				GUILayout.Label(PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.iOS), errorStyle, GUILayout.ExpandWidth(false));
			else
				GUILayout.Label(PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.iOS), goodStyle, GUILayout.ExpandWidth(false));

			GUILayout.Label("Android : ", GUILayout.ExpandWidth(false));
			if (string.IsNullOrEmpty(PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.Android)))
				GUILayout.Label("Not Set", errorStyle, GUILayout.ExpandWidth(false));
			else if (PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.Android) == "com.pinpinteam.gametemplate")
				GUILayout.Label(PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.Android), errorStyle, GUILayout.ExpandWidth(false));
			else
				GUILayout.Label(PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.Android), goodStyle, GUILayout.ExpandWidth(false));
		}
		EditorGUILayout.EndHorizontal();


		if (m_buildTargetList.m_projectPhase > BuildTargetList.ProjectPhase.Prototyping)
		{
			EditorGUILayout.BeginHorizontal();

			GUILayout.Label("Unity Services :");
			if (string.IsNullOrEmpty(CloudProjectSettings.projectId))
				GUILayout.Label("Not Connected", errorStyle, GUILayout.ExpandWidth(false));
			else
			{
				GUILayout.Label("Linked, ", goodStyle, GUILayout.ExpandWidth(false));
				if (CloudProjectSettings.projectName == PlayerSettings.productName)
					GUILayout.Label(CloudProjectSettings.projectName, goodStyle, GUILayout.ExpandWidth(false));
				else
				{
					GUILayout.Label("Names are not mathching : ", errorStyle, GUILayout.ExpandWidth(false));
					GUILayout.Label(CloudProjectSettings.projectName + "/" + PlayerSettings.productName, errorStyle, GUILayout.ExpandWidth(false));
				}
			}

			EditorGUILayout.EndHorizontal();

			bool tapNationLoaded = AssetDatabase.IsValidFolder("Assets/FunGamesSdk");
			if (!tapNationLoaded)
			{
				EditorGUILayout.BeginHorizontal();

				GUILayout.Label("TAPNATION Package :");
				GUILayout.Label("Not Loaded", errorStyle, GUILayout.ExpandWidth(false));
				if (GUILayout.Button("Import It", GUILayout.ExpandWidth(false)))
				{
					AssetDatabase.ImportPackage(Application.dataPath + "/Packages/TapNationPackage.unitypackage", false);
					//PinpinMenu.AddDefineSymbol("TAPNATION");
				}
				EditorGUILayout.EndHorizontal();
			}


			if (tapNationLoaded)
			{

				string androidSymbolsString = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android);
				string iosSymbolsString = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS);
				if (!androidSymbolsString.Contains(";TAPNATION") || !iosSymbolsString.Contains(";TAPNATION"))
				{
					EditorGUILayout.BeginHorizontal();
					GUILayout.Label("TAPNATION Define symbol :");
					GUILayout.Label("Missing", errorStyle, GUILayout.ExpandWidth(false));
					if (tapNationLoaded && GUILayout.Button("Add It", GUILayout.ExpandWidth(false)))
					{
						PinpinMenu.AddDefineSymbol("TAPNATION");
					}
					EditorGUILayout.EndHorizontal();
				}

#if TAPNATION
				EditorGUILayout.BeginHorizontal();
				GUILayout.Label("Google sheet url : ", GUILayout.ExpandWidth(false));

				m_googleSheetUrl = EditorGUILayout.TextField(m_googleSheetUrl);

				EditorGUILayout.EndHorizontal();

				EditorGUILayout.BeginHorizontal();
				if (GUILayout.Button("LoadIt", GUILayout.ExpandWidth(true)))
				{
					EditorCoroutineUtility.StartCoroutine(GoogleSheetDownloader.DownloadCSVCoroutine(m_googleSheetUrl, ( result ) =>
					{
						Debug.Log(result);

						// Read data from google sheet.
						List<List<string>> datas = GoogleSheetDownloader.ParseCSV(result);

						string GAiOSGameName = "";
						string GAAndroidGameName = "";
						for (int i = 0; i < datas.Count; i++)
						{
							switch (datas[i][0])
							{
								case "Bundle ID":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("Bundle ID : " + datas[i][2]);
										PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, datas[i][2]);
										PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS, datas[i][2]);
									}
									break;
								case "TokenAndroid":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("Adjust Android Token : " + datas[i][2]);
										m_androidAdjustToken = datas[i][2];
									}
									break;
								case "Token":
								case "TokenIOS":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("Adjust ios Token : " + datas[i][2]);
										m_iOSAdjustToken = datas[i][2];
									}
									break;
								case "Facebook ID":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("Facebook ID : " + datas[i][2]);
										FacebookSettings.AppIds.Clear();
										FacebookSettings.AppIds.Add(datas[i][2]);
										m_funGamesSettings.facebookGameID = datas[i][2];
										m_funGamesSettings.useFacebook = true;
									}
									break;
								case "App Name":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("App Name : " + datas[i][2]);
										PlayerSettings.productName = datas[i][2];
									}
									break;
								case "Game Analytics Game Name":
								case "Game Analytics iOS Game Name":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("Game Analytics iOS Game Name : " + datas[i][2]);
										GAiOSGameName = datas[i][2];
										m_funGamesSettings.useGameAnalytics = true;
									}
									break;
								case "Game Analytics Android Game Name":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("App Name : " + datas[i][2]);
										GAAndroidGameName = datas[i][2];
										m_funGamesSettings.useGameAnalytics = true;
									}
									break;
								case "AppLovin Max":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("AppLovin Max : " + datas[i][2]);
										m_funGamesSettings.maxSdkKey = datas[i][2];
										m_funGamesSettings.useMax = true;
									}
									break;
								case "Key Banner":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("Key Banner : " + datas[i][2]);
										m_funGamesSettings.iOSBannerAdUnitId = datas[i][2];
									}
									break;
								case "Key Interstitial":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("Key Interstitial : " + datas[i][2]);
										m_funGamesSettings.iOSInterstitialAdUnitId = datas[i][2];
									}
									break;
								case "Key RV":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("Key RV : " + datas[i][2]);
										m_funGamesSettings.iOSRewardedAdUnitId = datas[i][2];
									}
									break;
								case "Key Banner Android":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("Key Banner Android : " + datas[i][2]);
										m_funGamesSettings.androidBannerAdUnitId = datas[i][2];
									}
									break;
								case "Key Interstitial Android":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("Key Interstitial Android : " + datas[i][2]);
										m_funGamesSettings.androidInterstitialAdUnitId = datas[i][2];
									}
									break;
								case "Key RV Android":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("Key RV Android : " + datas[i][2]);
										m_funGamesSettings.androidRewardedAdUnitId = datas[i][2];
									}
									break;
								case "Ogury iOS Asset Key":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("Ogury iOS Asset Key : " + datas[i][2]);
										m_funGamesSettings.oguryIOSAssetKey = datas[i][2];
										m_funGamesSettings.useOgury = true;
									}
									break;
								case "Ogury iOS Thumbnail":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("Ogury iOS Thumbnail : " + datas[i][2]);
										m_funGamesSettings.iOSThumbnailAdUnitId = datas[i][2];
									}
									break;
								case "Ogury Android Asset Key":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("Ogury Android Asset Key : " + datas[i][2]);
										m_funGamesSettings.oguryAndroidAssetKey = datas[i][2];
									}
									break;
								case "Ogury Android Thumbnail":
									if (!string.IsNullOrEmpty(datas[i][2]))
									{
										Debug.Log("Ogury Android Thumbnail : " + datas[i][2]);
										m_funGamesSettings.androidThumbnailAdUnitId = datas[i][2];
									}
									break;
							}
						}
						EditorUtility.SetDirty(m_funGamesSettings);
						EditorUtility.SetDirty(FacebookSettings.Instance);
						m_gameAnalyticsSettings.PasswordGA = "pinpinProdGA";
						GA_SettingsInspector.LoginUser(m_gameAnalyticsSettings);
						EditorCoroutineUtility.StartCoroutine(WaitForGAData(GAiOSGameName, GAAndroidGameName), this);

					}), this);
				}
				EditorGUILayout.EndHorizontal();

				if (m_funGamesSettings.useFacebook)
				{
					EditorGUILayout.BeginHorizontal();
					GUILayout.Label("Facebook App ID :");

					if (!FacebookSettings.IsValidAppId)
					{
						GUILayout.Label("Invalid", errorStyle, GUILayout.ExpandWidth(false));
						if (tapNationLoaded && GUILayout.Button("Select Settings", GUILayout.ExpandWidth(false)))
						{
							Selection.activeObject = AssetDatabase.LoadAssetAtPath<FacebookSettings>("Assets/FacebookSDK/SDK/Resources/FacebookSettings.asset");
						}
					}
					else
					{
						GUILayout.Label(FacebookSettings.AppId, goodStyle, GUILayout.ExpandWidth(false));
					}
					EditorGUILayout.EndHorizontal();
				}

				if (m_funGamesSettings.useGameAnalytics)
				{
					EditorGUILayout.BeginHorizontal();
					GUILayout.Label("Game Analytics Settings :");


					GUILayout.Label("iOS :", GUILayout.ExpandWidth(false));
					if (string.IsNullOrEmpty(m_funGamesSettings.gameAnalyticsIosGameKey) || string.IsNullOrEmpty(m_funGamesSettings.gameAnalyticsIosSecretKey))
					{
						GUILayout.Label("Invalid", errorStyle, GUILayout.ExpandWidth(false));
					}
					else
					{
						GUILayout.Label("OK", goodStyle, GUILayout.ExpandWidth(false));
					}

					if (m_buildTargetList.m_projectPhase > BuildTargetList.ProjectPhase.CPITest)
					{
						GUILayout.Label("Android :", GUILayout.ExpandWidth(false));
						if (string.IsNullOrEmpty(m_funGamesSettings.gameAnalyticsAndroidGameKey) || string.IsNullOrEmpty(m_funGamesSettings.gameAnalyticsAndroidSecretKey))
						{
							GUILayout.Label("Invalid", errorStyle, GUILayout.ExpandWidth(false));
						}
						else
						{
							GUILayout.Label("OK", goodStyle, GUILayout.ExpandWidth(false));
						}
					}
					EditorGUILayout.EndHorizontal();
				}

				if (m_funGamesSettings.useOgury)
				{
					EditorGUILayout.BeginHorizontal();
					GUILayout.Label("Ogury Settings :");


					GUILayout.Label("iOS :", GUILayout.ExpandWidth(false));
					if (string.IsNullOrEmpty(m_funGamesSettings.oguryIOSAssetKey))
					{
						GUILayout.Label("Empty", errorStyle, GUILayout.ExpandWidth(false));
					}
					else
					{
						GUILayout.Label(m_funGamesSettings.oguryIOSAssetKey, goodStyle, GUILayout.ExpandWidth(false));
					}

					if (m_buildTargetList.m_projectPhase > BuildTargetList.ProjectPhase.CPITest)
					{
						GUILayout.Label("Android :", GUILayout.ExpandWidth(false));
						if (string.IsNullOrEmpty(m_funGamesSettings.oguryAndroidAssetKey))
						{
							GUILayout.Label("Empty", errorStyle, GUILayout.ExpandWidth(false));
						}
						else
						{
							GUILayout.Label(m_funGamesSettings.oguryAndroidAssetKey, goodStyle, GUILayout.ExpandWidth(false));
						}
					}
					EditorGUILayout.EndHorizontal();
				}

				if (m_funGamesSettings.useMax)
				{
					EditorGUILayout.BeginHorizontal();
					GUILayout.Label("Applovin MAX Settings :");


					GUILayout.Label("iOS :", GUILayout.ExpandWidth(false));
					if (string.IsNullOrEmpty(m_funGamesSettings.iOSInterstitialAdUnitId)|| string.IsNullOrEmpty(m_funGamesSettings.iOSRewardedAdUnitId) || string.IsNullOrEmpty(m_funGamesSettings.iOSBannerAdUnitId))
					{
						GUILayout.Label("Missing Ad Unit", errorStyle, GUILayout.ExpandWidth(false));
					}
					else
					{
						GUILayout.Label("OK", goodStyle, GUILayout.ExpandWidth(false));
					}

					if (m_buildTargetList.m_projectPhase > BuildTargetList.ProjectPhase.CPITest)
					{
						GUILayout.Label("Android :", GUILayout.ExpandWidth(false));
						if (string.IsNullOrEmpty(m_funGamesSettings.iOSInterstitialAdUnitId) || string.IsNullOrEmpty(m_funGamesSettings.iOSRewardedAdUnitId) || string.IsNullOrEmpty(m_funGamesSettings.iOSBannerAdUnitId))
						{
							GUILayout.Label("Missing Ad Unit", errorStyle, GUILayout.ExpandWidth(false));
						}
						else
						{
							GUILayout.Label("OK", goodStyle, GUILayout.ExpandWidth(false));
						}
					}
					EditorGUILayout.EndHorizontal();
				}
#endif
			}
		}

		EditorGUILayout.EndVertical();

		if (m_buildTargetList != null)
		{
			errorStyle.fontSize = 20;
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);
			GUILayout.Label("Current Build Target : " + PlayerPrefs.GetString("CurrentTarget", "None"), errorStyle);
			EditorGUILayout.BeginHorizontal();

			EditorGUI.BeginChangeCheck();

			m_buildTargetList.buildPath = EditorGUILayout.TextField(m_buildTargetList.buildPath);
			if (GUILayout.Button("Select folder", GUILayout.ExpandWidth(false)))
			{
				string newFolder = EditorUtility.OpenFolderPanel("Select build folder", m_buildTargetList.buildPath, "");
				if (newFolder.Length > 0)
					m_buildTargetList.buildPath = newFolder;
			}
			EditorGUILayout.EndHorizontal();


			EditorGUILayout.EndVertical();

			m_scrollPos = GUILayout.BeginScrollView(m_scrollPos, false, false, GUILayout.ExpandHeight(true));

			GUILayout.BeginVertical();
			for (int i = 0; i < m_buildTargetList.buildTargets.Count; i++)
			{
				if (DrawBuildTarget(m_buildTargetList.buildTargets[i]))
				{
					m_buildTargetList.buildTargets.RemoveAt(i);
					i--;
				}
			}
			GUILayout.EndVertical();
			if (GUILayout.Button("Add build target"))
			{
				m_buildTargetList.buildTargets.Add(new BuildTargetList.BuildTarget());
			}
			if (EditorGUI.EndChangeCheck())
			{
				EditorUtility.SetDirty(m_buildTargetList);
			}
			GUILayout.EndScrollView();
		}
		else
		{
			GUILayout.Label("Error : Missing BuildTargetList.asset", errorStyle);
			if (GUILayout.Button("Create It", GUILayout.ExpandWidth(true)))
			{
				m_buildTargetList = ScriptableObject.CreateInstance<Pinpin.BuildTargetList>();

				AssetDatabase.CreateFolder("Assets", "Objects");
				AssetDatabase.CreateAsset(m_buildTargetList, "Assets/Objects/BuildTargetList.asset");
				AssetDatabase.SaveAssets();

			}
		}
	}

	private bool DrawBuildTarget ( BuildTargetList.BuildTarget buildTarget )
	{
		bool destroy = false;
		EditorGUILayout.BeginVertical(EditorStyles.helpBox);
		{
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Label("Build target name : ");
				if (GUILayout.Button("Delete build target", GUILayout.ExpandWidth(false)))
				{
					destroy = true;
				}
			}
			EditorGUILayout.EndHorizontal();

			buildTarget.buildName = GUILayout.TextField(buildTarget.buildName);
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Label("Defines : ", GUILayout.ExpandWidth(false));
				EditorGUILayout.BeginVertical();
				{
					for (int i = 0; i < buildTarget.defines.Count; i++)
					{
						EditorGUILayout.BeginHorizontal();
						{
							buildTarget.defines[i] = EditorGUILayout.TextField(buildTarget.defines[i], GUILayout.ExpandWidth(true));
							if (GUILayout.Button("-", GUILayout.ExpandWidth(false)))
							{
								buildTarget.defines.RemoveAt(i);
								i--;
							}
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				EditorGUILayout.EndVertical();
			}
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Label("Build Android AAB : ", GUILayout.ExpandWidth(false));
				buildTarget.buildAndroidAAB = EditorGUILayout.Toggle(buildTarget.buildAndroidAAB, GUILayout.ExpandWidth(true));
			}
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			{
				if (GUILayout.Button("Add define", GUILayout.ExpandWidth(true)))
				{
					buildTarget.defines.Add("");
				}
				if (GUILayout.Button("Select Target", GUILayout.ExpandWidth(true)))
				{
					SetupTarget(buildTarget);
				}
				if (GUILayout.Button("Build", GUILayout.ExpandWidth(true)))
				{
					BuildTarget(buildTarget);
				}
			}
			EditorGUILayout.EndHorizontal();

		}
		GUILayout.EndVertical();

		return destroy;
	}

	private void SetupTarget ( Pinpin.BuildTargetList.BuildTarget buildTarget )
	{
		m_buildTargetList.OnValidate();
		foreach (string define in m_buildTargetList.allDefines)
		{
			PinpinMenu.RemoveDefineSymbol(define);
		}

		for (int i = 0; i < buildTarget.defines.Count; i++)
		{
			PinpinMenu.AddDefineSymbol(buildTarget.defines[i]);
		}
		PlayerPrefs.SetString("CurrentTarget", buildTarget.buildName);
	}

	private void BuildTarget ( Pinpin.BuildTargetList.BuildTarget buildTarget )
	{
		if (EditorUtility.DisplayDialog("Build", "Build current target : " + PlayerPrefs.GetString("CurrentTarget", "None"), "yes", "no"))
		{
			SetupTarget(buildTarget);
			List<string> scenes = new List<string>();
			for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
			{
				if (EditorBuildSettings.scenes[i].enabled)
				{
					scenes.Add(EditorBuildSettings.scenes[i].path);
				}
			}
			EditorUserBuildSettings.buildAppBundle = false;
			BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
			buildPlayerOptions.scenes = scenes.ToArray();
			buildPlayerOptions.locationPathName = m_buildTargetList.buildPath + "/"
												+ Application.productName + "-"
												+ buildTarget.buildName + "-"
												+ Application.version + "-"
												+ (EditorUserBuildSettings.activeBuildTarget == UnityEditor.BuildTarget.iOS ? PlayerSettings.iOS.buildNumber : PlayerSettings.Android.bundleVersionCode.ToString())
												+ (EditorUserBuildSettings.activeBuildTarget == UnityEditor.BuildTarget.Android ? ".apk" : "");

			buildPlayerOptions.target = EditorUserBuildSettings.activeBuildTarget;
			buildPlayerOptions.options = BuildOptions.None;

			BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
			BuildSummary summary = report.summary;

			if (summary.result == BuildResult.Succeeded)
			{
				Debug.Log("Build succeeded: " + summary.totalSize + " bytes");

				if (buildTarget.buildAndroidAAB && EditorUserBuildSettings.activeBuildTarget == UnityEditor.BuildTarget.Android)
				{
					EditorUserBuildSettings.buildAppBundle = true;
					buildPlayerOptions.locationPathName = m_buildTargetList.buildPath + "/"
														+ Application.productName + "-"
														+ buildTarget.buildName + "-"
														+ Application.version + "-"
														+ PlayerSettings.Android.bundleVersionCode.ToString()
														+ ".aab";
					report = BuildPipeline.BuildPlayer(buildPlayerOptions);
					summary = report.summary;

					if (summary.result == BuildResult.Succeeded)
					{
						Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
						if (EditorUserBuildSettings.activeBuildTarget == UnityEditor.BuildTarget.Android)
							PlayerSettings.Android.bundleVersionCode++;
						OpenInFileBrowser.Open(report.summary.outputPath);
					}

					if (summary.result == BuildResult.Failed)
					{
						Debug.Log("Build failed");
					}

				}
				else
				{
					if (EditorUserBuildSettings.activeBuildTarget == UnityEditor.BuildTarget.Android)
						PlayerSettings.Android.bundleVersionCode++;
					OpenInFileBrowser.Open(report.summary.outputPath);
				}
			}

			if (summary.result == BuildResult.Failed)
			{
				Debug.Log("Build failed");
			}
		}

	}

}
