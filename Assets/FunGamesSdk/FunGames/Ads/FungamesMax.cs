using System;
using FunGames.Sdk.Analytics;
using FunGamesSdk;
using FunGamesSdk.FunGames.Ads;
using GameAnalyticsSDK;
using UnityEngine;

public class FunGamesMax
{
	private static string _maxSdkKey;
	private static string _interstitialAdUnitId;
	private static string _rewardedAdUnitId;
	private static string _bannerAdUnitId;

	private static int _interstitialRetryAttempt;
	private static int _rewardedRetryAttempt;
	private static int _bannerRetryAttempt;

	private static Action<string, string, int> _rewardedCallback;
	private static string _rewardedCallbackArgString;
	private static int _rewardedCallbackArgInt;

	private static Action<string, string, int> _interstitialCallback;
	private static string _interstitialCallbackArgString;
	private static int _interstitialCallbackArgInt;

	private static bool _isBannerLoaded;
	private static bool _showBannerAsked;
	private static bool _isBannerShowing;

	private static float _timeSinceLastAd = -1f;

	public static float timeSinceLastAd
	{
		get { return (_timeSinceLastAd); }
	}


	// Awake is called on the awake of FunGamesAds
	internal static void Awake ()
	{
		var settings = FunGamesSettings.settings;
		_maxSdkKey = settings.maxSdkKey;

#if UNITY_IOS
		_interstitialAdUnitId = settings.iOSInterstitialAdUnitId;
		_rewardedAdUnitId = settings.iOSRewardedAdUnitId;
		_bannerAdUnitId = settings.iOSBannerAdUnitId;
#endif

#if UNITY_ANDROID
		_interstitialAdUnitId = settings.androidInterstitialAdUnitId;
		_rewardedAdUnitId = settings.androidRewardedAdUnitId;
		_bannerAdUnitId = settings.androidBannerAdUnitId;
#endif
	}

	// Start is called on the start of FunGamesAds
	internal static void Start ( Pinpin.UI.GDPRPopup _GDPRPopup = null )
	{
		var settings = FunGamesSettings.settings;
		if (settings.useMax)
		{
			MaxSdk.SetSdkKey(_maxSdkKey);
			MaxSdkCallbacks.OnSdkInitializedEvent += sdkConfiguration =>
			{
				if (PlayerPrefs.HasKey("GDPRAnswered"))
				{
					MaxSdk.SetHasUserConsent(PlayerPrefs.GetInt("GDPRAnswered") == 1);
					InitializeAds();
				}
				else if (!settings.useOgury && _GDPRPopup != null)
				{
					if (sdkConfiguration.ConsentDialogState == MaxSdkBase.ConsentDialogState.Applies)
					{
						_GDPRPopup.gameObject.SetActive(true);
						_GDPRPopup.onGDPRAnswered += OnGDPRPopupAnswered;

						// Show user consent dialog
					}
					else if (sdkConfiguration.ConsentDialogState == MaxSdkBase.ConsentDialogState.DoesNotApply)
					{
						// No need to show consent dialog, proceed with initialization
						InitializeAds();
					}
					else
					{
						// Consent dialog state is unknown. Proceed with initialization, but check if the consent
						// dialog should be shown on the next application initialization
						InitializeAds();
					}
				}
				else
				{
					InitializeAds();
				}
			};
			MaxSdk.InitializeSdk();
			Debug.Log("Initializing FunGamesAds");
		}
	}

	private static void OnGDPRPopupAnswered ( bool answer )
	{
		PlayerPrefs.SetInt("GDPRAnswered", answer ? 1 : 0);
		MaxSdk.SetHasUserConsent(answer);
		InitializeAds();
	}

	private static void InitializeAds ()
	{
		InitializeInterstitialAds();
		InitializeRewardedAds();
		InitializeBannerAds();
	}

	public static void Update ()
	{
		if (_timeSinceLastAd >= 0f)
			_timeSinceLastAd += Time.deltaTime;
	}

	private static void InitializeInterstitialAds ()
	{
		MaxSdkCallbacks.OnInterstitialLoadedEvent += OnInterstitialLoadedEvent;
		MaxSdkCallbacks.OnInterstitialLoadFailedEvent += OnInterstitialFailedEvent;
		MaxSdkCallbacks.OnInterstitialAdFailedToDisplayEvent += InterstitialFailedToDisplayEvent;
		MaxSdkCallbacks.OnInterstitialDisplayedEvent += OnInterstitialDisplayedEvent;
		MaxSdkCallbacks.OnInterstitialHiddenEvent += OnInterstitialDismissedEvent;
		try
		{
			LoadInterstitial();
		}
		catch
		{
			Debug.Log("Failed Load Interstitials : Please Check Ad Unit");
		}
	}

	private static void LoadInterstitial ()
	{
		MaxSdk.LoadInterstitial(_interstitialAdUnitId);
	}

	private static void OnInterstitialLoadedEvent ( string adUnitId )
	{
		_interstitialRetryAttempt = 0;
	}

	private static void OnInterstitialFailedEvent ( string adUnitId, int errorCode )
	{
		_interstitialRetryAttempt++;
		var retryDelay = Math.Pow(2, _interstitialRetryAttempt);

		FunGamesAds._instance.Invoke(nameof(LoadInterstitial), (float)retryDelay);
		FunGamesAnalytics.NewAdEvent(GAAdAction.FailedShow, GAAdType.Interstitial);
		_interstitialCallback?.Invoke("fail", _interstitialCallbackArgString, _interstitialCallbackArgInt);
		_interstitialCallback = null;
	}

	private static void OnInterstitialDisplayedEvent ( string adUnitId )
	{
		FunGamesAnalytics.NewAdEvent(GAAdAction.Show, GAAdType.Interstitial);
		_interstitialCallback?.Invoke("success", _interstitialCallbackArgString, _interstitialCallbackArgInt);
		_interstitialCallback = null;
		_timeSinceLastAd = 0f;
	}

	private static void InterstitialFailedToDisplayEvent ( string adUnitId, int errorCode )
	{
		LoadInterstitial();
		FunGamesAnalytics.NewAdEvent(GAAdAction.FailedShow, GAAdType.Interstitial);
		_interstitialCallback?.Invoke("fail", _interstitialCallbackArgString, _interstitialCallbackArgInt);
		_interstitialCallback = null;
	}

	private static void OnInterstitialDismissedEvent ( string adUnitId )
	{
		LoadInterstitial();
		FunGamesAnalytics.NewDesignEvent("Interstitial", "Dismissed");
	}

	private static void InitializeRewardedAds ()
	{
		MaxSdkCallbacks.OnRewardedAdLoadedEvent += OnRewardedAdLoadedEvent;
		MaxSdkCallbacks.OnRewardedAdLoadFailedEvent += OnRewardedAdFailedEvent;
		MaxSdkCallbacks.OnRewardedAdFailedToDisplayEvent += OnRewardedAdFailedToDisplayEvent;
		MaxSdkCallbacks.OnRewardedAdDisplayedEvent += OnRewardedAdDisplayedEvent;
		MaxSdkCallbacks.OnRewardedAdClickedEvent += OnRewardedAdClickedEvent;
		MaxSdkCallbacks.OnRewardedAdHiddenEvent += OnRewardedAdDismissedEvent;
		MaxSdkCallbacks.OnRewardedAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;

		try
		{
			LoadRewardedAd();
		}
		catch
		{
			Debug.Log("Failed Load Rewarded : Please Check Ad Unit");
		}
	}

	private static void LoadRewardedAd ()
	{
		MaxSdk.LoadRewardedAd(_rewardedAdUnitId);
	}

	private static void OnRewardedAdLoadedEvent ( string adUnitId )
	{
		_rewardedRetryAttempt = 0;
		FunGamesAnalytics.NewAdEvent(GAAdAction.Loaded, GAAdType.RewardedVideo);
	}

	private static void OnRewardedAdFailedEvent ( string adUnitId, int errorCode )
	{
		_rewardedRetryAttempt++;
		var retryDelay = Math.Pow(2, _rewardedRetryAttempt);

		FunGamesAds._instance.Invoke("LoadRewardedAd", (float)retryDelay);
		FunGamesAnalytics.NewAdEvent(GAAdAction.FailedShow, GAAdType.RewardedVideo);
		_rewardedCallback?.Invoke("fail", _rewardedCallbackArgString, _rewardedCallbackArgInt);
		_rewardedCallback = null;
	}

	private static void OnRewardedAdDisplayedEvent ( string adUnitId )
	{
		FunGamesAnalytics.NewAdEvent(GAAdAction.Show, GAAdType.RewardedVideo);
		_timeSinceLastAd = 0f;
	}

	private static void OnRewardedAdFailedToDisplayEvent ( string adUnitId, int errorCode )
	{
		LoadRewardedAd();
		FunGamesAnalytics.NewAdEvent(GAAdAction.FailedShow, GAAdType.RewardedVideo);
		_rewardedCallback?.Invoke("fail", _rewardedCallbackArgString, _rewardedCallbackArgInt);
		_rewardedCallback = null;
	}

	private static void OnRewardedAdClickedEvent ( string adUnitId )
	{
		FunGamesAnalytics.NewAdEvent(GAAdAction.Clicked, GAAdType.RewardedVideo);
	}

	private static void OnRewardedAdDismissedEvent ( string adUnitId )
	{
		LoadRewardedAd();
		_rewardedCallback?.Invoke("fail", _rewardedCallbackArgString, _rewardedCallbackArgInt);
		_rewardedCallback = null;
		_timeSinceLastAd = 0f;
	}

	private static void OnRewardedAdReceivedRewardEvent ( string adUnitId, MaxSdk.Reward reward )
	{
		FunGamesAnalytics.NewAdEvent(GAAdAction.RewardReceived, GAAdType.RewardedVideo);
		_rewardedCallback?.Invoke("success", _rewardedCallbackArgString, _rewardedCallbackArgInt);
		_rewardedCallback = null;
		_timeSinceLastAd = 0f;
	}

	private static void InitializeBannerAds ()
	{
		MaxSdkCallbacks.OnBannerAdLoadedEvent += BannerIsLoaded;

		try
		{
			MaxSdk.CreateBanner(_bannerAdUnitId, MaxSdkBase.BannerPosition.BottomCenter);
			MaxSdk.SetBannerBackgroundColor(_bannerAdUnitId, Color.black);
		}
		catch
		{
			Debug.Log("Failed Create Banner : Please Check Ad Unit");
		}
	}

	internal static void BannerIsLoaded ( string adUnitId )
	{
		_isBannerLoaded = true;

		if (_showBannerAsked)
		{
			ShowBannerAd();
		}
	}

	internal static void ShowBannerAd ()
	{
		if (_isBannerShowing)
		{
			return;
		}

		if (_showBannerAsked == false)
		{
			_showBannerAsked = true;
		}

		if (_isBannerLoaded == false)
		{
			return;
		}

		MaxSdk.ShowBanner(_bannerAdUnitId);
		_isBannerShowing = true;
		FunGamesAnalytics.NewAdEvent(GAAdAction.Show, GAAdType.Banner);
	}

	internal static void HideBannerAd ()
	{
		if (_isBannerLoaded == false)
		{
			return;
		}

		MaxSdk.HideBanner(_bannerAdUnitId);
		_isBannerShowing = false;
		_showBannerAsked = false;
	}

	internal static bool IsRewardedAdReady ()
	{
		return MaxSdk.IsRewardedAdReady(_rewardedAdUnitId);
	}

	internal static bool IsInterstitialAdReady ()
	{
		return MaxSdk.IsInterstitialReady(_interstitialAdUnitId);
	}

	internal static void LoadAds ()
	{
		ShowBannerAd();
		LoadRewardedAd();
		LoadInterstitial();
	}

	internal static void ShowRewarded ( Action<string, string, int> callback, string callbackArgsString = "", int callbackArgsInt = 0 )
	{
		_rewardedCallback += callback;
		_rewardedCallbackArgString = callbackArgsString;
		_rewardedCallbackArgInt = callbackArgsInt;

		if (MaxSdk.IsRewardedAdReady(_rewardedAdUnitId))
		{
			try
			{
				MaxSdk.ShowRewardedAd(_rewardedAdUnitId);
				FunGamesAnalytics.NewDesignEvent("Rewarded" + callbackArgsString, "succeeded");
				callback?.Invoke("succeeded", callbackArgsString, callbackArgsInt);
			}
			catch (Exception e)
			{
				callback?.Invoke("fail", callbackArgsString, callbackArgsInt);
				FunGamesAnalytics.NewDesignEvent("RewardedError" + callbackArgsString, "UserQuitBeforeEndingAd");
				Debug.Log(e);
				throw;
			}
		}
		else
		{
			callback?.Invoke("fail", callbackArgsString, callbackArgsInt);
			FunGamesAnalytics.NewDesignEvent("RewardedNoAd" + callbackArgsString, "NoAdReady");
			_rewardedCallback = null;
		}
	}

	internal static void ShowInterstitial ( Action<string, string, int> callback, string callbackArgsString = "", int callbackArgsInt = 0 )
	{
		_interstitialCallback += callback;
		_interstitialCallbackArgString = callbackArgsString;
		_interstitialCallbackArgInt = callbackArgsInt;

		if (MaxSdk.IsInterstitialReady(_interstitialAdUnitId))
		{
			try
			{
				MaxSdk.ShowInterstitial(_interstitialAdUnitId);
			}
			catch (Exception e)
			{
				callback?.Invoke("fail", callbackArgsString, callbackArgsInt);
				FunGamesAnalytics.NewDesignEvent("Error", "UserQuitBeforeEndingAd");
				Debug.Log(e);
				throw;
			}
		}
		else
		{
			callback?.Invoke("fail", callbackArgsString, callbackArgsInt);
			_interstitialCallback = null;
		}
	}
}
