﻿Shader "Hidden/OutlineDepth"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
		Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
		    #pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

            sampler2D _MainTex;
			float2 _ScreenRes;
			int _Width;
			float _DepthThreshold;
			float _ThresholdScale;
			float4 _Color;
			uniform sampler2D _CameraDepthNormalsTexture;
			float4x4 _ClipToView;
			float _OuterOnly;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 viewSpaceDir : TEXCOORD2;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.viewSpaceDir = mul(_ClipToView, o.vertex).xyz;

				return o;
			}

			float3 DecodeErrorDelta(float2 uv, float refDepth, float threshold)
			{
				float4 depthnormal = tex2D(_CameraDepthNormalsTexture, uv);
				float3 normal;
				float depth;
				DecodeDepthNormal(depthnormal, depth, normal);

				return abs(depth - refDepth) > threshold;
			}

            fixed4 frag (v2f input) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, input.uv);
				
				float4 depthnormal = tex2D(_CameraDepthNormalsTexture, input.uv);
				float3 normal;
				float depth;
				DecodeDepthNormal(depthnormal, depth, normal);

				float3 viewNormal = normal * 2 - 1;
				float NdotV = 1 - dot(viewNormal, -input.viewSpaceDir);
				float normalThreshold = saturate(NdotV) * _ThresholdScale + 1;
				float depthThreshold = _DepthThreshold * depth * normalThreshold;

				float3 edge = 0;
				int width = ceil(_Width * (1 - depth));

				for (int i = 0; i < _Width; i++)
				{
					for (int j = 0; j < _Width; j++)
					{
						if (edge.r != 1 || _Width >= width)
						{
							edge += DecodeErrorDelta(float2(input.uv.x + (i - width / 2.0) / _ScreenRes.x, input.uv.y + (j - width / 2.0) / _ScreenRes.y), depth, depthThreshold);
							edge = min(edge, 1);
						}
					}
				}

				return fixed4(col * (1- edge) + edge * _Color, 1);
            }
            ENDCG
        }
    }
}
