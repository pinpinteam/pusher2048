﻿using UnityEngine;
using UnityEditor;
using System.IO;
using UnityEngine.Rendering;

namespace Pinpin.Editor
{
	[CustomEditor(typeof(VolumeSpawner))]
	public class VolumeSpawnerEditor : UnityEditor.Editor
	{
		public override void OnInspectorGUI ()
		{
			base.OnInspectorGUI();

			VolumeSpawner s = (VolumeSpawner)target;

			if (GUILayout.Button("Spawn Prefabs"))
			{
				s.Spawn();
				EditorUtility.SetDirty(s.gameObject);
			}
		}
	}
}