﻿using UnityEngine;
using Pinpin.UI;

namespace Pinpin.Scene.MainScene.UI
{
	public sealed class WinPopup : AClosablePopup
	{
		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}

		protected override void OnClose ()
		{
			base.OnClose();

			FXManager.onFadeInFinishedAction += OnFadeInLaunchNextLevel;
			FXManager.Instance.FadeIn();
		}

		void OnFadeInLaunchNextLevel ()
		{
			GamePanel gp = UIManager.GetPanel<GamePanel>();
			int levelIndex = ApplicationManager.datas.level % ApplicationManager.assets.levels.Length;
			gp.Init(levelIndex);
			GameManager.Instance.LoadLevel(levelIndex);

			FXManager.Instance.FadeOut();
			// UIManager.OpenPanel<MainPanel>();
			// UIManager.sceneManager.gameManager.RemoveCurrentLevel();
		}

	}

}