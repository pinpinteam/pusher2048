﻿using Pinpin.UI;
using UnityEngine;
using UnityEngine.UI;
using Pinpin.Helpers;
using DG.Tweening;

namespace Pinpin.Scene.MainScene.UI
{
	public sealed class GamePanel : AUIPanel
	{
		[SerializeField] private RectTransform m_topContainer;
		[SerializeField] private RectTransform m_bottomContainer;

		private float m_topContainerYPos;
		private float m_bottomContainerYPos;

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}

		public override bool OnBackAction()
		{
			return false;
		}

		protected override void Awake ()
		{
			base.Awake();
			GameManager.OnLevelEnd += OnLevelEnd;

			m_topContainerYPos = m_topContainer.anchoredPosition.y;
			m_bottomContainerYPos = m_bottomContainer.anchoredPosition.y;
		}

		void OnEnable ()
		{
			m_topContainer.anchoredPosition = new Vector2(0, m_topContainerYPos + 200f);
			m_bottomContainer.anchoredPosition = new Vector2(0, m_bottomContainerYPos - 200f);

			m_topContainer.DOAnchorPosY(m_topContainerYPos, 0.5f).SetEase(Ease.OutQuad);
			m_bottomContainer.DOAnchorPosY(m_bottomContainerYPos, 0.5f).SetEase(Ease.OutQuad);
		}

		public void Init ( int levelIndex )
		{
		}

		void OnLevelEnd ( bool win )
		{
			if (win)
			{
				UIManager.OpenPopup<WinPopup>();
			}
			else
			{
				UIManager.OpenPopup<LosePopup>();
			}
		}
	}
}