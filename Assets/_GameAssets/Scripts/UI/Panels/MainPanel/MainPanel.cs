﻿using AdsManagement;
using Pinpin.UI;
using UnityEngine;
using UnityEngine.UI;
using Pinpin.Helpers;
using DG.Tweening;

using UVector3 = UnityEngine.Vector3;
using System;
using System.Collections;
using URandom = UnityEngine.Random;
using System.Collections.Generic;

namespace Pinpin.Scene.MainScene.UI
{
	public sealed class MainPanel : AUIPanel
	{
		#region members

		[SerializeField] private bool m_chooseLevel;
		[SerializeField] private Canvas m_cheatCanvas;
		[SerializeField] PushButton m_startButton;
		[SerializeField] Camera m_mainPanelCamera;

		[Header("PlaceHolderUI")]
		[SerializeField] PushButton m_levelBtnPrefab;
		[SerializeField] Transform m_levelBtnParent;
		[SerializeField] GameObject m_scrollViewParent;

		private int m_currentLevelIndex;

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}
		#endregion

		#region MonoBehavior
		protected override void Awake ()
		{
			base.Awake();
			CreateLevelBtns();
		}

		void CreateLevelBtns()
		{
			for (int i = 0; i < ApplicationManager.assets.levels.Length; i++)
			{
				int levelIndex = i;
				PushButton p = Instantiate(m_levelBtnPrefab, m_levelBtnParent);
				p.text = ApplicationManager.assets.levels[i].levelName.ToUpper();
				p.onClick += () =>{ LaunchLevel(levelIndex); };
			}
		}

		void Start ()
		{
			//ApplicationManager.onRewardedVideoAvailabilityChange += OnRewardedVideoAvailabilityChange;

			m_startButton.onClick += OnStartButtonClick;

#if CAPTURE || UNITY_EDITOR
			m_cheatCanvas.gameObject.SetActive(true);
#else
			Destroy(m_cheatCanvas.gameObject);
#endif
		}

		private void OnEnable ()
		{
			UIManager.ShowTopCanvas<MoneyTopCanvas>();

			m_mainPanelCamera.gameObject.SetActive(true);
			m_startButton.gameObject.SetActive(true);
			m_scrollViewParent.SetActive(false);
		}

		protected override void OnDestroy ()
		{
			base.OnDestroy();
			//ApplicationManager.onRewardedVideoAvailabilityChange -= OnRewardedVideoAvailabilityChange;
		}
		#endregion

		#region Buttons CallBacks

		private void OnRewardedVideoAvailabilityChange ( bool value )
		{

		}

		private void OnStartButtonClick ()
		{
			m_startButton.gameObject.SetActive(false);
			if (m_chooseLevel)
			{
				m_scrollViewParent.SetActive(true);
			}
			else
			{
				LaunchCurrentLevel();
			}
		}

		private void LaunchCurrentLevel()
		{
			LaunchLevel(ApplicationManager.datas.level % ApplicationManager.assets.levels.Length);
		}
		void LaunchLevel(int index)
		{
			m_currentLevelIndex = index;

			UIManager.HideTopCanvas<MoneyTopCanvas>();

			FXManager.onFadeInFinishedAction += OnFadeInLaunchLevel;
			FXManager.Instance.FadeIn();
		}

		void OnFadeInLaunchLevel()
		{
			m_mainPanelCamera.gameObject.SetActive(false);

			GamePanel gp = UIManager.OpenPanel<GamePanel>();
			gp.Init(m_currentLevelIndex);
			UIManager.sceneManager.gameManager.LoadLevel(m_currentLevelIndex);
			
			FXManager.Instance.FadeOut();
		}

		#endregion
	}
}