﻿using PaperPlaneTools;
using Pinpin.Helpers;
using Pinpin.Scene.MainScene.UI;
using System;
using System.Collections;
using UnityEngine;
using MoreMountains.NiceVibrations;
#if TAPNATION
using FunGames.Sdk.Analytics;
#endif

namespace Pinpin
{

	public class GameManager : Singleton<GameManager>
	{

		#region members
		public static Action<bool> OnLevelEnd;

		private Level m_currentLevel;
		#endregion

		#region MonoBehavior
		private new void Awake ()
		{
			//ApplicationManager.onRewardedVideoAvailabilityChange += OnRewardedVideoAvailabilityChange;
			base.Awake();
		}

		// Use this for initialization
		void Start ()
		{
		}

		private void OnDestroy ()
		{
			//ApplicationManager.onRewardedVideoAvailabilityChange -= OnRewardedVideoAvailabilityChange;
		}
		private void Update ()
		{
			if (Input.GetKeyDown(KeyCode.T))
				ApplicationManager.datas.haveCompleteTutorial = false;
		}

		#endregion

		private void OnRewardedVideoAvailabilityChange ( bool value )
		{

		}

		#region Level Callbacks
		public void LevelWon ()
		{
#if TAPNATION
			FunGamesAnalytics.NewProgressionEvent("Complete", ApplicationManager.datas.level.ToString());
#endif
			ApplicationManager.datas.level++;

			OnLevelEnd.Invoke(true);

			MMVibrationManager.Haptic(HapticTypes.Success);


			FXManager.Instance.PlayConffeti();
		}

		public void LevelFailed ()
		{
#if TAPNATION
			FunGamesAnalytics.NewProgressionEvent("Fail", ApplicationManager.datas.level.ToString());
#endif

			MMVibrationManager.Haptic(HapticTypes.Failure);

			OnLevelEnd.Invoke(false);
		}
		#endregion

		#region GAME
		public void RemoveCurrentLevel ()
		{
			if (m_currentLevel != null)
				Destroy(m_currentLevel.gameObject);
		}

		public void LoadLevel ( int levelIndex )
		{
			StartCoroutine(LoadLevelCoroutine(levelIndex));
		}

		private IEnumerator LoadLevelCoroutine ( int levelIndex )
		{
			RemoveCurrentLevel();

			yield return null;

			m_currentLevel = Instantiate(ApplicationManager.assets.levels[levelIndex]);
#if TAPNATION
			FunGamesAnalytics.NewProgressionEvent("Start", ApplicationManager.datas.level.ToString());
#endif
		}
		#endregion

	}

}
