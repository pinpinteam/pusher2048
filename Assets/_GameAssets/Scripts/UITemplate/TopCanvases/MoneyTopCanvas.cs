﻿using DG.Tweening;
using Pinpin.Helpers;
using Pinpin.UI;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin
{
	public class MoneyTopCanvas : AUITopCanvas
	{
		[SerializeField] private Text m_coinText;
		[SerializeField] private Text m_hardText;
		[Space()]
		[SerializeField] private RectTransformPositionsLerp m_posLerp;
		[SerializeField] private RectTransformPositionsLerp m_starPosLerp;
		[Space()]
		[SerializeField] private AutoScaleLoop m_IconAutoScaleLoop;
		[SerializeField] private AutoScaleLoop m_TextAutoScaleLoop;
		[SerializeField] private ParticleSystem m_coinParticleSystem;
		[Space()]
		[SerializeField] private AutoScaleLoop m_hardIconScaleLoop;
		[SerializeField] private AutoScaleLoop m_hardTextScaleLoop;
		[SerializeField] private ParticleSystem m_hardParticleSystem;
		[Space()]
		[SerializeField] private ShakeRectTfm m_coinShakeTfm;
		[SerializeField] private ShakeRectTfm m_gemShakeTfm;


		private float m_startCoin;
		private float m_incCoin;
		private float m_currentCoin;

		private float m_startHard;
		private float m_incHard;
		private float m_currentHard;

		[Space()]
		[SerializeField] private float m_timer;
		private float m_coinT;
		private float m_hardT;

		public override bool Visible
		{
			get
			{
				return m_visible;
			}
			set
			{
				m_visible = value;
				m_posLerp.SetPos(m_visible ? 1 : 0, false);
				InstantUpdateCoinDisplay();
			}
		}

		public bool StarVisible
		{
			get
			{
				return m_starVisible;
			}
			set
			{
				m_starVisible = value;
				m_starPosLerp.SetPos(m_starVisible ? 1 : 0, false);
			}
		}
		private bool m_starVisible = false;

		private Sequence m_bounceSequence;

		private void Start()
		{
			//StarVisible = false;
			m_coinT = 99f;
			m_hardT = 99f;

			m_currentCoin = ApplicationManager.datas.softCurrency;
			InstantUpdateCoinDisplay();

			ApplicationManager.onCoinsChange += UpdateCoinsDisplay;
			ApplicationManager.onHardChange += UpdateHardDisplay;
			//GameDatas.onLoadComplete += ShowCoinDisplay;
		}

		private void OnDestroy()
		{
			ApplicationManager.onCoinsChange -= UpdateCoinsDisplay;
			ApplicationManager.onHardChange -= UpdateHardDisplay;
			//GameDatas.onLoadComplete -= ShowCoinDisplay;
		}

		public void InstantUpdateCoinDisplay()
		{
			m_coinText.text = MathHelper.ConvertToEngineeringNotation(ApplicationManager.datas.softCurrency, 3);
			m_hardText.text = " "+ApplicationManager.datas.hardCurrency.ToString();
		}

		public void UpdateCoinsDisplay(ulong oldCoin, ulong currentCoin)
		{
			m_startCoin = (float) oldCoin;
			m_incCoin = (float)currentCoin - (float)oldCoin;
			m_timer = 1.75f;

			if (m_coinT < m_timer)
			{
				m_coinT = 0f;

				if (m_incCoin > 0)
					m_coinParticleSystem.Play();
				else
				{
					m_timer = 1f;
					m_coinParticleSystem.Stop();
				}
			}
			else
			{
				m_coinT = 0f;

				if (m_incCoin > 0)
					m_coinParticleSystem.Play();
				else
				{
					m_timer = 1f;
					m_coinParticleSystem.Stop();
				}

				StartCoroutine(UpdateCoinDisplayCoroutine());
			}

			//m_starsText.text = ApplicationManager.datas.stars.ToString();
		}

		IEnumerator UpdateCoinDisplayCoroutine()
		{
			m_IconAutoScaleLoop.StartLoop();
			m_TextAutoScaleLoop.StartLoop();

			while (m_coinT < m_timer)
			{
				m_currentCoin = (ulong)(m_startCoin + (m_incCoin * (m_coinT/m_timer)));

				m_coinText.text = MathHelper.ConvertToEngineeringNotation((ulong)m_currentCoin, 3);

				yield return null;
				m_coinT += Time.deltaTime;
			}

			m_coinT = 99f;

			m_coinText.text = MathHelper.ConvertToEngineeringNotation(ApplicationManager.datas.softCurrency, 3);

			m_IconAutoScaleLoop.StopLoop();
			m_TextAutoScaleLoop.StopLoop();
			m_coinParticleSystem.Stop();
		}

		public void UpdateHardDisplay(ulong oldHard, ulong currentHard)
		{
			m_startHard = (float)oldHard;
			m_incHard = (float)currentHard - (float)oldHard;
			m_timer = 1.75f;

			if (m_hardT < m_timer)
			{
				m_hardT = 0f;

				if (m_incHard > 0)
				{
					if (m_incHard == 1)
					{
						m_timer = 0.5f;
					}
					m_hardParticleSystem.Play();
				}
				else
				{
					m_hardParticleSystem.Stop();
					m_timer = 1f;
				}
			}
			else
			{
				m_hardT = 0f;

				if (m_incHard > 0)
				{
					if(m_incHard == 1)
					{
						m_timer = 0.5f;
					}
					m_hardParticleSystem.Play();
				}
				else
				{
					m_hardParticleSystem.Stop();
					m_timer = 1f;
				}

				StartCoroutine(UpdateHardDisplayCoroutine());
			}

			//m_starsText.text = ApplicationManager.datas.stars.ToString();
		}

		IEnumerator UpdateHardDisplayCoroutine()
		{
			m_hardIconScaleLoop.StartLoop();
			m_hardTextScaleLoop.StartLoop();

			while (m_hardT < m_timer)
			{
				m_currentHard = (ulong)(m_startHard + (m_incHard * (m_hardT / m_timer)));

				m_hardText.text = MathHelper.ConvertToEngineeringNotation((ulong)m_currentHard, 3);

				yield return null;
				m_hardT += Time.deltaTime;
			}

			m_hardT = 99f;

			m_hardText.text = MathHelper.ConvertToEngineeringNotation(ApplicationManager.datas.hardCurrency, 3);

			m_hardIconScaleLoop.StopLoop();
			m_hardTextScaleLoop.StopLoop();
			m_hardParticleSystem.Stop();
		}

		public void ShakeCoinDisplay()
		{
			m_coinShakeTfm.Shake();
		}

		public void ShakeGemDisplay ()
		{
			m_gemShakeTfm.Shake();
		}

		/*public void UpdateStarDisplay()
		{
			m_starText.text = " " + ApplicationManager.datas.hardCurrencies.ToString();
			m_starParticleSystem.Stop();
			m_starParticleSystem.Play();
			m_starIconBounce.Bounce();
			m_starTextBounce.Bounce();
		}*/
	}
}
