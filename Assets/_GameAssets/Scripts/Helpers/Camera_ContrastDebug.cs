﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Camera_ContrastDebug : MonoBehaviour
{
	private Material material;
	[SerializeField] [Range(0, 1)] private float m_threshold;
	[SerializeField] [Range(1, 5)] private int m_width;
	[SerializeField] private Color m_color;

	private void Awake ()
	{
		material = new Material(Shader.Find("Hidden/ContrastDebugger"));
	}

	void Reset ()
	{
		material = new Material(Shader.Find("Hidden/ContrastDebugger"));
	}

	void OnRenderImage ( RenderTexture source, RenderTexture destination )
	{
		SetParameters();

		if (material == null)
			return;

		Graphics.Blit(source, destination, material);	
	}

	private void SetParameters ()
	{
		material.SetFloat("_Width", m_width);
		material.SetFloat("_Threshold", m_threshold);
		material.SetVector("_ScreenRes", new Vector2(Screen.width, Screen.height));
		material.SetColor("_Color", m_color);
	}
}
