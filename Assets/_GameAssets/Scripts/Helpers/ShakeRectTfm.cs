﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeRectTfm : MonoBehaviour
{
	[SerializeField] private RectTransform rect;
	[SerializeField] private AnimationCurve shakeXCurve;
	[SerializeField] private float force;
	[SerializeField] private float speed;

	private float t;
	private Vector2 newPos;

	private Vector2 startLocalPos;

	private void Awake ()
	{
		startLocalPos = rect.anchoredPosition;
		newPos = startLocalPos;
	}

	private void OnEnable ()
	{
		rect.anchoredPosition = startLocalPos;
		t = 0f;
	}

	public void Shake ()
	{
		if (gameObject.activeSelf)
		{
			if (t == 0f)
			{
				StartCoroutine(ShakeCoroutine());
			}
			else
			{
				t = 0f;
			}
		}
	}

	IEnumerator ShakeCoroutine ()
	{
		while (t < 1f)
		{
			t += Time.deltaTime * speed;
			newPos.x = startLocalPos.x + shakeXCurve.Evaluate(t) * force;
			rect.anchoredPosition = newPos;
			yield return null;
		}

		t = 0f;
	}
}
