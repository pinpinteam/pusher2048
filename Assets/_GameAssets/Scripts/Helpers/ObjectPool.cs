﻿using System.Collections.Generic;
using UnityEngine;

public delegate void PoolEvent(IPooledObject caller);

public interface IPooledObject
{
    void OnAdmit(float objectIndex, float objectsCount);
    void OnDismiss();

    GameObject gameObject { get; }

    event PoolEvent AskToDismiss;
}

public class ObjectPool
{
    public int m_Size;
    public bool m_DynamicSize;
    public bool m_Recycle = true;

    protected List<GameObject> m_models;
    protected List<GameObject> m_pooledObjects;
    protected List<GameObject> m_usedObjects;
    protected Transform m_parent;

    public GameObject Model
    {
        get { return m_models[0]; }
        set
        {
            if (m_models == null)
                m_models = new List<GameObject>();
            m_models.Add(value);
        }
    }

    public GameObject[] UsedObjects
    {
        get { return m_usedObjects.ToArray();  }
    }

    public ObjectPool(List<GameObject> models, int size = 0, bool dynamicSize = false, Transform parent = null)
    {
        m_models = models;
        m_Size = Mathf.Max(m_models.Count, size);
        m_DynamicSize = dynamicSize;
        m_parent = parent;

        CreatePool();
    }

    public ObjectPool(GameObject[] models, int size = 0, bool dynamicSize = false, Transform parent = null) :
                      this(new List<GameObject>(models), size, dynamicSize)
    { }


    public ObjectPool(GameObject model, int size, bool dynamicSize = false, Transform parent = null)
    {
        Model = model;
        m_Size = size;
        m_DynamicSize = dynamicSize;
        m_parent = parent;

        CreatePool();
    }

    public virtual void CreatePool()
    {
        m_usedObjects = new List<GameObject>();
        m_pooledObjects = new List<GameObject>(m_Size);

        for (int i = 0; i < m_Size; ++i)
            AddToPool(m_models[i % m_models.Count]);
    }

    protected GameObject AddToPool(GameObject model)
    {
        GameObject pooled;

        if (m_parent == null)
            pooled = GameObject.Instantiate(model);
        else
            pooled = GameObject.Instantiate(model, m_parent);
        pooled.SetActive(false);
        m_pooledObjects.Add(pooled);

        return pooled;
    }

    protected GameObject Get(int poolIndex)
    {
        GameObject instance;
        IPooledObject contract;

        instance = null;
        if (m_pooledObjects.Count > poolIndex)
            instance = m_pooledObjects[poolIndex];
        else if (m_DynamicSize)
            instance = AddToPool(m_models[Random.Range(0, m_models.Count)]);
        else if (m_Recycle)
        {
            instance = m_usedObjects[0];
            Dismiss(instance);
        }

        if (instance == null)
            return null;

        m_usedObjects.Add(instance);
        m_pooledObjects.Remove(instance);

        contract = instance.GetComponent<IPooledObject>();
        if (contract != null)
        {
            contract.AskToDismiss += PooledObject_AskToDismiss;
            contract.OnAdmit(poolIndex, m_pooledObjects.Count);
        }
        else
            instance.SetActive(true);

        return instance;
    }

    protected void PooledObject_AskToDismiss(IPooledObject caller)
    {
        caller.AskToDismiss -= PooledObject_AskToDismiss;
        Dismiss(caller.gameObject);
    }

    public virtual GameObject Get()
    {
        return Get(0);
    }

    public virtual GameObject GetRandom()
    {
        return Get(Random.Range(0, m_pooledObjects.Count));
    }

    public void Dismiss(GameObject instance)
    {
        IPooledObject contract;

        if (m_usedObjects.Remove(instance))
            m_pooledObjects.Add(instance);

        contract = instance.GetComponent<IPooledObject>();
        if (contract != null)
            contract.OnDismiss();
        else
            instance.SetActive(false);

    }

    public void DismissAll()
    {
        while (m_usedObjects.Count > 0)
        {
            Dismiss(m_usedObjects[0]);
        }
    }
}
