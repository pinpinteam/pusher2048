﻿using System.Globalization;
using UnityEngine;

namespace Pinpin.Helpers
{

	public static class MathHelper
	{

		public static float MapBetween ( float value, float valueMin, float valueMax, float resultMin, float resultMax )
		{
			return (resultMax - resultMin) / (valueMax - valueMin) * (value - valueMax) + resultMax;
		}

		private static string[] EndLetters = new string[] { "", "K", "M", "B", "T", "Qa", "Qi", "Sx", "Sp", "Oc", "No", "Dc", "Un", "Du" };

		public static string ConvertToEngineeringNotation ( ulong value, int significantNumbers = 6, int decimalCount = 3)
		{
			string rawValue = value.ToString();
			int lenght = rawValue.Length;
			if (lenght > significantNumbers)
			{
				int thousandMultiplier = (lenght - 1) / 3;
				int thousandNumbers = lenght % 3;

				if (thousandNumbers == 0)
					thousandNumbers = 3;
				string resultString;
				resultString = new string(rawValue.ToCharArray(0, thousandNumbers));
				if (significantNumbers - thousandNumbers > 0)
					resultString += "." + new string(rawValue.ToCharArray(thousandNumbers, Mathf.Min(significantNumbers - thousandNumbers, decimalCount)));
				float result = float.Parse(resultString, CultureInfo.InvariantCulture);
				return result.ToString(ApplicationManager.currentCulture.NumberFormat) + EndLetters[thousandMultiplier];
			}
			else
			{
				return int.Parse(rawValue).ToString(ApplicationManager.currentCulture.NumberFormat);
			}
		}


		public static ulong Fibonnaci (int lv)
		{
			if (lv == 0)
				return 1;
			ulong baseScore = 1;
			ulong score = 0;
			ulong temp = 0;
			for (int i = 0; i < lv; i++)
			{
				temp = score;
				score = baseScore;
				baseScore += temp;
			}
			return score;
		}

	}

}