﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroy : MonoBehaviour
{
	[SerializeField] private float timer = 0f;

	private void OnEnable ()
	{
		if (timer != 0f)
		{
			StartCoroutine(DestroyCoroutine());
		}
	}

	IEnumerator DestroyCoroutine ()
	{
		float t = 0f;

		while (t < timer)
		{
			t += Time.deltaTime;
			yield return null;
		}

		Destroy(gameObject);
	}

	public void DestroyOnFinishAE ()
	{
		Destroy(gameObject);
	}
}
