﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class OutlineEffect : MonoBehaviour
{
	private Material material;
	[SerializeField] private Camera m_camera;

	[Space]
	[SerializeField, Range(0, 32)] private int m_width = 16;
	private float m_depthThreshold = 1f;
	private float m_thresholdScale = 4;
	[SerializeField] private Color m_color;

	void OnEnable ()
	{
		SetCameraDepth();
	}

	void OnValidate ()
	{
		SetCameraDepth();
	}

	void SetCameraDepth ()
	{
		if (m_camera == null)
			return;

		m_camera.depthTextureMode |= DepthTextureMode.DepthNormals;
	}

	private void Awake ()
	{
		material = new Material(Shader.Find("Hidden/OutlineDepth"));
	}

	void Reset ()
	{
		material = new Material(Shader.Find("Hidden/OutlineDepth"));
	}

	// Postprocess the image
	void OnRenderImage ( RenderTexture source, RenderTexture destination )
	{
		SetParameters();

		if (material == null)
			return;

		Graphics.Blit(source, destination, material);
	}

	void SetParameters ()
	{
		material.SetFloat("_Width", m_width);
		material.SetFloat("_DepthThreshold", m_depthThreshold);
		material.SetFloat("_ThresholdScale", m_thresholdScale);
		material.SetColor("_Color", m_color);
		material.SetVector("_ScreenRes", new Vector2(Screen.width, Screen.height));

		if (m_camera != null)
		{
			Matrix4x4 clipToView = GL.GetGPUProjectionMatrix(m_camera.projectionMatrix, true).inverse;
			material.SetMatrix("_ClipToView", clipToView);
		}	
	}
}
