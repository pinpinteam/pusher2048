using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Pinpin
{
	public class SquareSpawner : MonoBehaviour
	{
		[SerializeField] private GameObject m_prefab;
		[SerializeField] private Transform m_parent;
		[SerializeField] private int m_count;
		[SerializeField] private Vector2 m_size;
		[SerializeField] private float m_sphereRadius;

		public void Spawn ()
		{
#if UNITY_EDITOR
			float xStep;
			float zStep;

			if (m_size.x > m_size.y)
			{
				xStep = m_size.x / (m_count / m_size.y);
				zStep = m_sphereRadius*2f;
			}
			else
			{
				zStep = m_size.y / (m_count / m_size.x);
				xStep = m_sphereRadius * 2f;
			}

			Vector2 offset = new Vector2(transform.position.x - m_size.x / 2f + xStep / 2f, transform.position.z - m_size.y / 2f + zStep / 2f);
			int count = m_count;

			for (float x = 0f; x < m_size.x && count > 0; x += xStep)
			{
				for (float z = 0f; z < m_size.y && count > 0; z += zStep)
				{
					count--;
					GameObject go = PrefabUtility.InstantiatePrefab(m_prefab,m_parent) as GameObject;
					go.transform.position = transform.TransformPoint(x + offset.x, transform.position.y, z + offset.y);
				}
			}
#endif
		}

#if UNITY_EDITOR
		[SerializeField] private bool m_drawZoneGizmos;
		[SerializeField] private bool m_drawUnitsGizmos;
		[SerializeField] private Color m_gizmosColor;

		private void OnDrawGizmos ()
		{
			if (m_drawUnitsGizmos)
			{
				float xStep;
				float zStep;
				int count = m_count;

				if (m_size.x > m_size.y)
				{
					xStep = m_size.x / (count / m_size.y);
					zStep = m_sphereRadius * 2f;
				}
				else
				{
					zStep = m_size.y / (count / m_size.x);
					xStep = m_sphereRadius * 2f;
				}

				Vector2 offset = new Vector2(transform.position.x - m_size.x / 2f + xStep / 2f, transform.position.z - m_size.y / 2f + zStep / 2f);

				for (float x = 0f; x < m_size.x && count > 0; x += xStep)
				{
					for (float z = 0f; z < m_size.y && count > 0; z += zStep)
					{
						count--;
						Gizmos.DrawSphere(transform.TransformPoint(x + offset.x, transform.position.y, z + offset.y) , m_sphereRadius);
					}
				}
			}

			if (!m_drawZoneGizmos)
				return;

			Gizmos.color = m_gizmosColor;
			Gizmos.DrawCube(transform.position, new Vector3(m_size.x, 1f, m_size.y));
		}

		private void OnValidate ()
		{
			if (m_size.x <= 0.1f)
				m_size.x = 0.1f;

			if (m_size.y <= 0.1f)
				m_size.y = 0.1f;
		}
#endif
	}
}
