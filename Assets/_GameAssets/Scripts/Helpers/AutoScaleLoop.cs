﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoScaleLoop : MonoBehaviour
{
	[SerializeField] private bool loopOnEnable;

	[SerializeField] private Vector3[] scales;
	[SerializeField] private AnimationCurve curve;
	[SerializeField] private float speed;

	private bool active;

	private void OnEnable()
	{
		transform.localScale = scales[0];
		if (loopOnEnable || active)
		{
			transform.localScale = scales[0];
			StartLoop();
		}
	}

	public void StartLoop()
	{
		active = true;
		if (isActiveAndEnabled)
		{
			StopAllCoroutines();
			StartCoroutine(AnimateCoroutine());
		}
	}

	public void StopLoop()
	{
		active = false;
	}

	IEnumerator AnimateCoroutine()
	{
		while (active)
		{
			transform.localScale = Vector3.Lerp(scales[0], scales[1], curve.Evaluate(Time.time * speed));
			yield return null;
		}

		float t = 1f;

		while (t > 0f)
		{
			transform.localScale = Vector3.Lerp(transform.localScale, scales[0], 8f * Time.deltaTime);
			t -= Time.deltaTime;
			yield return null;
		}

		transform.localScale = scales[0];
	}
}
